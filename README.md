#  Examen técnico

## Como compilar y correr

####  Mac OS X
Si sos usuario de mac os x entonces es fácil, simplemente importas el proyecto al xcode y luego corres el proyecto ( Product > Run, o cmd+R).
Si no tenés la consola abierta entonces puedes abrirla utilizando View > Debug Area > Activate console, o con el comando Shift+cmd+C.

#### Linux ( Ubuntu / Debian)
Si te gusta la vieja escuela o simplemente no te gusta usar IDE's, este proyecto tiene un makefile. Simplemente en una terminal nos ubicamos en la carpeta 'Examen tecnico'  (donde se encuentra el Makefile) y luego utilizamos el comando: $ make
Luego, para correr el programa usamos: $ ./main

## Programa
El programa tiene una interfaz muy fácil de utilizar. Simplemente escribimos en la consola el número de la opción que queremos utilizar y luego apretamos ENTER para seleccionarla. Las opciones son:
1 - Para utilizar el telégrafo
2 - Para decodificar binario a MORSE
3 - Para decodificar MORSE a lenguaje
9 - Salir

Al finalizar la ejecución de cada opción el menú volverá a aparecer. En caso de que la opción no funcione correctamente se pide al usuario volver a introducir el número y ENTER.



