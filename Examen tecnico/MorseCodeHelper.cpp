//
//  MorseCode.cpp
//  Examen tecnico
//
//  Created by Andrés Lasso.
//  Copyright © 2019 Andrés Lasso. All rights reserved.
//

#include "MorseCodeHelper.h"

using namespace std;

MorseCodeHelper::MorseCodeHelper() {
    
    //Defaults
    sizeOfSpace = 3;
    sizeOfLetterSpace = 6;
    sizeOfDah = 3;
    
    codes = {
        {".-","A"}, {"-...","B"}, {"-.-.","C"}, {"----","CH"}, {"-..","D"}, {".","E"}, {"..-.","F"}, {"--.","G"},
        {"....","H"}, {"..","I"}, {".---","J"}, {"-.-","K"}, {".-..","L"}, {"--","M"}, {"-.","N"}, {"--.--","Ñ"},
        {"---","O"}, {".--.","P"}, {"--.-","Q"},{".-.","R"}, {"...","S"}, {"-","T"}, {"..-","U"}, {"...-","V"},
        {".-","W"}, {"-..-","X"}, {"-.--","Y"}, {"--..","X"}, {"-----","0"}, {".----","1"}, {"..---","2"},
        {"...--","3"}, {"....-","4"}, {".....","5"}, {"-....","6"}, {"--...","7"}, {"---..","8"}, {"----.","9"},
        {".-.-.-","."}, {"-.-.--",","},{"..--..","?"},
        {" ",""}, //Letter space
        {"  "," "}, {"   "," "}, {"    "," "}//Word space
    };
}

void MorseCodeHelper::analizeBinaryCode(const unsigned int bits[], const unsigned int bitsSize){
    bool lastOneIsOff = true;
    
    unsigned int arraySize = bitsSize;
    unsigned int countOfCeros[arraySize];
    unsigned int countOfOnes[arraySize];
    
    for(unsigned int i=0; i<arraySize; i++){
        countOfCeros[i]=0;
        countOfOnes[i]=0;
    }
    
    unsigned int quantity = 0;
    for(unsigned int i=0; i<bitsSize; i++){
        if(bits[i]==1){
            if(lastOneIsOff){
                countOfCeros[quantity]++;
                lastOneIsOff = !lastOneIsOff;
                quantity = 1;
            }else
                quantity++;
        } else {
            if(lastOneIsOff)
                quantity++;
            else{
                countOfOnes[quantity]++;
                lastOneIsOff = !lastOneIsOff;
                quantity = 1;
            }
        }
    }
    
    /**
     * Con el siguiente código trato de obtener la mayor parte de la campana de gauss de la cantidad de bits
     * que mide un dit,dah, espacio de códigos, letras y palabras. Busco el número que encierre la mayor
     * parte de esa campana teniendo cuidado que si las campans de cantidad de bits que miden los diferentes
     * simbolos no se superpongan demaciado.
     */
    
    /** Obtengo el máximo de cada distribución de bits agrupados */
    vector<unsigned int> maximusOfOnes = getIndicesOfLocalMaximums(countOfOnes, arraySize);
    
    if(maximusOfOnes.size()>=2){
        //Seteo el rango maximo de un dit
        sizeOfDit = getIndexOfMinimumBetween(countOfOnes, arraySize, maximusOfOnes[0], maximusOfOnes[1]);
        
        //Seteo el rango máximo de un dah
        if(maximusOfOnes.size()>2 && maximusOfOnes[2] < arraySize ){
            sizeOfDah = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfOnes[1], arraySize);
        }else
            sizeOfDah = maximusOfOnes[1]+CANT_BITS_DESVIO;
    }
    
    /** Obtengo el máximo de cada distribución de bits agrupados */
    vector<unsigned int> maximusOfCeros = getIndicesOfLocalMaximums(countOfCeros, arraySize);
    
    if(maximusOfCeros.size()>=3){
        //Seteo el rango maximo de espacio entre dit & dah
        sizeOfSpace = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfCeros[0], maximusOfCeros[1]);
        
        //Seteo el rango máximo de espacio entre letras
        sizeOfLetterSpace = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfCeros[1], maximusOfCeros[2]);
        
        //Seteo el rango máximo de espacio entre palabras
        if(maximusOfCeros.size()>3 &&  maximusOfCeros[3] < arraySize ){
            sizeOfWordSpace = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfCeros[2], arraySize);
        }else
            sizeOfWordSpace = maximusOfCeros[2]+CANT_BITS_DESVIO;
    }else{
        //Seteo el rango maximo de espacio entre dit & dah
        sizeOfSpace = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfCeros[0], maximusOfCeros[1]);
        
        //Seteo el rango máximo de espacio entre letras
        sizeOfLetterSpace = getIndexOfMinimumBetween(countOfCeros, arraySize, maximusOfCeros[1], arraySize);
    }
}

/**
 * Requiere: codeSize > 0
 */
string MorseCodeHelper::binaryToCode(const bool isCero, const unsigned int codeSize){
    MorseCodeIdentifier ret;
    if(isCero){
        if(codeSize <= sizeOfSpace)
            ret = space;
        else if(codeSize <= sizeOfLetterSpace)
            ret = letterSpace;
        else
            ret = wordSpace;
    }else{
        if(codeSize <= sizeOfDit)
            ret = dit;
        else
            ret = dah;
    }
    return identifyCode(ret);
}

string MorseCodeHelper::identifyCode(MorseCodeHelper::MorseCodeIdentifier code){
    string ret = "";
    switch (code) {
        case dit:
            ret = MORSE_CODE_DIT;
            break;
        case dah:
            ret = MORSE_CODE_DAH;
            break;
        case space:
            ret = MORSE_CODE_SPACE;
            break;
        case letterSpace:
            ret = MORSE_CODE_LETTER_SPACE;
            break;
        case wordSpace:
            ret = MORSE_CODE_WORD_SPACE;
            break;
    }
    return ret;
}

string MorseCodeHelper::decodeMorseCodeLetter(string code){
    if(codes.find(code) != codes.end())
        return codes[code];
    else
        return "";
}

vector<unsigned int> MorseCodeHelper::getIndicesOfLocalMaximums(const unsigned int nums[], const unsigned int size){
    vector<unsigned int> maximums;

    unsigned int localMaximum=nums[0];
    unsigned int localMaximumIndex=0;
    
    for(int i=1; i<size; i++){
        
        if(nums[i] >= localMaximum){
            if(localMaximum!=0 && localMaximum>nums[i-1])
                maximums.push_back(localMaximumIndex);
            localMaximum = nums[i];
            localMaximumIndex = i;
        }else if(nums[i-1]<nums[i]){
            if(localMaximum!=0)
                maximums.push_back(localMaximumIndex);
            localMaximum = nums[i];
            localMaximumIndex = i;
        }
        
        if(i==size-1 && localMaximum!=0){
            maximums.push_back(localMaximumIndex);
        }
    }
    
    return maximums;
}

unsigned int MorseCodeHelper::getIndexOfMinimumBetween(const unsigned int nums[], const unsigned int size, const unsigned int a, const unsigned int b) {
    unsigned int minIndx=a;
    if(a<b && b<=size){
        //unsigned int middle = b-a<=2 ? 0 : (b-a)/2;
        unsigned int middle = (b-a)/2;
        unsigned int middleBetweenAB = a+middle;
        unsigned int i = a;
        while(i<middleBetweenAB){
            i++;
            if(nums[minIndx]>nums[i])
                minIndx = i;
        }
    }
    return minIndx;
}


