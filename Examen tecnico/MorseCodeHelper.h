//
//  MorseCode.h
//  Examen tecnico
//
//  Created by Andrés Lasso.
//  Copyright © 2019 Andrés Lasso. All rights reserved.
//

#ifndef MorseCodeHelper_h
#define MorseCodeHelper_h

#include <string>
#include <map>
#include <stdio.h>
#include <vector>
#include <cstdlib>

using namespace std;

const string MORSE_CODE_DIT = ".";
const string MORSE_CODE_DAH = "-";
const string MORSE_CODE_SPACE = "";
const string MORSE_CODE_LETTER_SPACE = " ";
const string MORSE_CODE_WORD_SPACE = "  ";
const unsigned int CANT_BITS_DESVIO = 2;

/**
 * Ayuda a decifrar código binario a morse y morse a palabras
 */
class MorseCodeHelper {
    
public:
    
    /**
     * Constructor
     */
    MorseCodeHelper();

    /**
     * Analiza el código binario para setear cuantos bits representan un dit o dah
     * (si el bit esta prendido) o un espacio de código morse entre dit,dah , letras
     * o palabras en codigo (si el bit esta apagado).
     */
    void analizeBinaryCode(const unsigned int bits[], const unsigned int bitsSize);
    
    /**
     * Devuelve el código de la agrupación de 1 y 0 representan.
     */
    string binaryToCode(const bool isCero, const unsigned int codeSize);
    
    /**
     * Devuelve, si existe, el símbolo que representa el código morse.
     * Si no existe ningún simbolo devolverá un string vacío
     */
    string decodeMorseCodeLetter(string code);
    
    /**
     * Devuelve true si y solo si es un código morse.
     * Es decir, devolverá false si el mismo tiene un espacio.
     */
    static bool isMorseLetter(char code){
        return code=='.' || code=='-';
    }
    
private:
    enum MorseCodeIdentifier { dit, dah, space, letterSpace, wordSpace };
    
    /**
     * Propiedades para la identificación binaria de código morse
     * Se interpretan como la cantidad de 0|1 que repesenta cada código.
     */
    unsigned int sizeOfSpace;
    unsigned int sizeOfLetterSpace;
    unsigned int sizeOfWordSpace;
    unsigned int sizeOfDit;
    unsigned int sizeOfDah;
    map<string, string> codes;
    
    /**
     * Devuelve el código morse al que esta asociado el CodeIdentifier
     * Es decir, la traducción a string.
     */
    string identifyCode(MorseCodeIdentifier code);
    
    /**
     * Devuelve los índices de las posiciones en que el arreglo tiene un maximo local.
     * Si hay varios numeros maximos juntos, el indice el de mayor indice
     */
    vector<unsigned int> getIndicesOfLocalMaximums(const unsigned int nums[], const unsigned int size);
    
    /**
     * Devuelve el indice de la posicion con el menor numero entre los numeros de las
     * posiciones a y b.
     */
    unsigned int getIndexOfMinimumBetween(const unsigned int nums[], const unsigned int size, const unsigned int a, const unsigned int b);
    
};


#endif /* MorseCodeHelper_h */
