//
//  main.cpp
//  Examen técnico
//
//  Created by Andrés Lasso.
//  Copyright © 2019 Andrés Lasso. All rights reserved.
//

#include <iostream>
#include <string>
#include <sstream>
#include "MorseCodeHelper.h"
#include "KeyboardHelper.hpp"

using namespace std;

string decodeBits2Morse (unsigned int bits[], unsigned int bitsSize);
string translate2Human (const string morseMessage);
void binaryStringToArray(const string binaryString, unsigned int binaryArray[], const unsigned int size);

int main(int argc, const char * argv[]) {
    int opcion = 0;
    string opcionStr;
    
    do {
        cout<<"Precione los números según la opción deseada:" << endl;
        cout<<"1 - Para utilizar el telégrafo" << endl;
        cout<<"2 - Para decodificar binario a MORSE" << endl;
        cout<<"3 - Para decodificar MORSE a lenguaje" << endl;
        cout<<"9 - Salir" << endl;
        
        //cin.ignore();
        //cin >> opcion;
        opcionStr = "";
        getline (cin,opcionStr);
        stringstream(opcionStr) >> opcion;
        opcionStr = "";
        
        switch (opcion) {
            case 9: {
                cout<<"Finalizando programa" << endl;
                break;
            }
            case 1: {
                
                cout<<"Pulsá la barra espaciadora para emitir DITs."<<endl;
                string binaryStringCode;
                
                set_conio_terminal_mode();
                
                int tecla=0;
                while(tecla!=27 && tecla!=13){
                
                    int sensibilidad = 0;
                    while (!kbhit()) {
                        if(sensibilidad==99999){
                            if(tecla==131)
                                tecla=-1;
                            else if(tecla==-1)
                                tecla=0;
                            else{
                                cout << "0";
                                binaryStringCode += "0";
                            }
                            sensibilidad=0;
                        }else
                            sensibilidad++;
                    }
                    
                    tecla = getch(); /* consume the character */
                    //if(tecla==32)
                    if(tecla==131){
                        binaryStringCode += "1";
                        cout << "1";
                    }
                    //cout << "Tecla:" << tecla << endl;
                }
                
                reset_terminal_mode();
                
                cout << "Binario: "<< binaryStringCode << endl;
                
                unsigned int arraySize = (int)binaryStringCode.length();
                unsigned int binaryArrayCode[arraySize];
                binaryStringToArray(binaryStringCode, binaryArrayCode, arraySize);
                string morseCode = decodeBits2Morse(binaryArrayCode, arraySize);
                cout<<"Morse: " <<  morseCode << endl;
                string traduccion = translate2Human(morseCode);
                cout<<"Traducción: " <<  traduccion << endl;
                
                break;
            }
            case 2: {
                string binaryStringCode;
                
                //TEST:
                //000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000
                
                cout<<"Ingrese el código binario, al terminar presionar enter.\n" ;
                getline(cin,binaryStringCode);
                
                unsigned int arraySize = (int)binaryStringCode.length();
                unsigned int binaryArrayCode[arraySize];
                
                binaryStringToArray(binaryStringCode, binaryArrayCode, arraySize);
                
                string binaryDecoded = decodeBits2Morse(binaryArrayCode, arraySize);
                cout<<"Morse: " <<  binaryDecoded << endl;
                break;
            }
            case 3: {
                string morseCode;

                //TEST:
                //.... --- .-.. .-  -- . .-.. ..
                
                cout<<"Ingrese el código morse, al terminar presionar enter.\n" ;
                getline(cin,morseCode);
                
                string morseDecoded = translate2Human(morseCode);
                cout<<"Traducción: " <<  morseDecoded << endl;
                break;
            }
            default:
                opcion = 0;
                cout << "La opción ingresada no es correcta." << endl;
                break;
        }
        
    }while (opcion!=9);
    
    return 0;
}

/******************   Implementación decodeMorseCode   ********************/

/**
 * Require: bits contenga 1 o 0. bitsSize = sizeOf(bits). bitsSize>0
 */
string decodeBits2Morse ( unsigned int bits[], unsigned int bitsSize){
    string bitsDecoded = "";
    MorseCodeHelper morse;
    
    if(bitsSize > 0){
        morse.analizeBinaryCode(bits, bitsSize);
        
        bool lastOneIsOff = true;
        unsigned int quantity = 0;
        for(unsigned int i=0; i<bitsSize; i++){
            if(bits[i]==1){
                if(lastOneIsOff){
                    bitsDecoded += morse.binaryToCode(lastOneIsOff, quantity);
                    lastOneIsOff = !lastOneIsOff;
                    quantity = 1;
                }else{
                    quantity++;
                }
            } else {
                if(lastOneIsOff){
                    quantity++;
                }else{
                    bitsDecoded += morse.binaryToCode(lastOneIsOff, quantity);
                    lastOneIsOff = !lastOneIsOff;
                    quantity = 1;
                }
            }
        }
        
        bitsDecoded += morse.binaryToCode(lastOneIsOff, quantity);
    }
    
    return bitsDecoded;
}

/******************   Implementación translate2Human   ********************/

string translate2Human (const string morseCode){
    string message;
    
    MorseCodeHelper morse;
    
    string code = "";
    bool isMorseCode = false;
    char c;
    for(unsigned int i=0; i<morseCode.length(); i++) {
        c = morseCode[i];
        if(MorseCodeHelper::isMorseLetter(c)){
            if(isMorseCode){
                code += c;
            }else{
                isMorseCode = true;
                message += morse.decodeMorseCodeLetter(code);
                code = c;
            }
        }else{
            if(isMorseCode){
                isMorseCode = false;
                message += morse.decodeMorseCodeLetter(code);
                code = c;
            }else{
                code += c;
            }
        }
    }
    
    message += morse.decodeMorseCodeLetter(code);
    
    return message;
}


/******************   Auxiliares   ********************/

void binaryStringToArray(const string binaryString, unsigned int binaryArray[], const unsigned int size) {
    for(unsigned int i=0; i<size; i++)
        binaryArray[i] = binaryString[i] - '0'; //Normalizo número restandole donde empiezan los números ASCII
}



